package com.sda.sorting;

//import com.sun.xml.internal.bind.v2.model.annotation.Quick;

import java.util.*;

public class Main {
    public static void main(String[] args) {
//        int[] tablica = new int[] { 10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
//        int[] tablica = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int[] tablica = new int[]{1,3,5,7,9,10,8,6,4,2};
//        int[] tablica = new int[10];
//        for (int i = 0; i < 10; i++) {
//            tablica[i] = new Random().nextInt();
//        }

//        BubbleSort.sort(tablica);
//        CountingSort.sort(tablica);
//        InsertionSort.sort(tablica);
//        MergeSort.sort(tablica);
        QuickSort.sort(tablica);

        // n*n = 45
        // n*logn = 34
        // n = 9
        System.out.println(Arrays.toString(tablica));

//        Arrays.sort(tablica);
//        Collections.sort();
    }
}

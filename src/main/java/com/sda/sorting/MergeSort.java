package com.sda.sorting;

import java.util.Arrays;

public class MergeSort {
    private static int counter = 0;

    public static void sort(int[] tablica) {
        // posortuj od indeksu 0 do length-1
        counter = 0;
        mergeSort(tablica, 0, tablica.length - 1);
        System.out.println(counter);
    }

    private static void mergeSort(int[] tablica, int from, int to) {
        if (from == to) {
            return;
        }

        int middle = (to + from) / 2;  // środek zakresu (nie środek tablicy)

        mergeSort(tablica, from, middle); // podział lewej strony
        mergeSort(tablica, middle + 1, to); // podział prawej strony

        // połącz dwie połowy w całość
        // przekazujemy parametry żeby znać pozycję podziału
        merge(tablica, from, middle, to);
    }

    private static void merge(int[] tablica, int from, int middle, int to) {

        int[] copy = Arrays.copyOf(tablica, tablica.length);

        int indexLeft = from;
        int indexRight = middle + 1;
        int pozycjaWstawiania = from;
        // jeśli są elementy w obu podtablicach
        // porównujemy element z tablicy lewej i prawej - dopóki jest co porównywać
        while (indexLeft <= middle && indexRight <= to) {
            counter++;
            if (copy[indexLeft] >= copy[indexRight]) {
                tablica[pozycjaWstawiania++] = copy[indexLeft++];
            } else if (copy[indexLeft] < copy[indexRight]) {
                tablica[pozycjaWstawiania++] = copy[indexRight++];
            }
//            pozycjaWstawiania++;
        }

        while (indexLeft <= middle) {
            counter++;
            tablica[pozycjaWstawiania++] = copy[indexLeft++];
        }
        while (indexRight <= to) {
            counter++;
            tablica[pozycjaWstawiania++] = copy[indexRight++];
        }
    }
}
